import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActionService, DashboardService, LoginService, MenuService} from '../../_api';
import {Router} from '@angular/router';
import {AuthIdentityService, ToastrService} from '../../_services';
import swal from 'sweetalert2';

declare  var $;
@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css'],
  providers: [MenuService, DashboardService, ActionService],
})
export class LeftSidebarComponent implements OnInit, AfterViewInit {

  mainMenuList: any;
  onLineClient: string;
  user: any;
  searchUser: any;
  cUserData: any;
  searchUserData: any;
  searchUserError = false;

  // tslint:disable-next-line:max-line-length
  constructor(
      private service: MenuService,
      private service2: DashboardService,
      private service3: ActionService ) {
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    //  console.log(this.user);
    }
  }

  ngOnInit() {
    this.getMainMenuData();
  }

  ngAfterViewInit() {
  }

  async getMainMenuData() {
    await this.service.getList().subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      (data) => {
        this.onSuccess(data);
      },
      error => {
        // this.toaster.error('Error in Get MenuList Api !!', 'Something want wrong..!');
      });
  }

  onSuccess(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.mainMenuList = response.data.list;
        this.onLineClient = response.data.onLineClient;
      }
    }
  }

  allUserLogout() {
    swal.fire({
      title: 'Logout All Users?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.allUserLogout().subscribe((res) => this.onSuccessUserLogout(res));
      }
    });
  }

  onSuccessUserLogout(res) {
    if (res.status === 1) {
      this.getMainMenuData();
    }
  }

  changeClass(id, title, selectedId, closeMenu) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).addClass('active');
    if (title === 'Users') {
        $('.inActivegroup').toggle();
        $('.reportsgroup').hide();
        $('.manage-system').hide();
    } else if (title === 'Reports') {
        $('.reportsgroup').toggle();
        $('.inActivegroup').hide();
        $('.manage-system').hide();
    } else if (title === 'White Label') {
        $('.manage-system').toggle();
        $('.inActivegroup').hide();
        $('.reportsgroup').hide();
    } else {
        $('.reportsgroup').hide();
        $('.inActivegroup').hide();
        $('.manage-system').hide();
    }

    if ( closeMenu ){
      $('.topnav-menu').click();
    }
  }

  changeSubClass(selectedId) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).addClass('active');

    $('.topnav-menu').click();
  }

  submitForm() {
    this.searchUser = $('.user-search-val2').val();
    if (this.searchUser) {
      const data = { username: this.searchUser };
      this.service2.searchUserData(data).subscribe((res) => this.onSearch(res));
    }
  }

  onSearch(res) {
    $('.modal-search-mobile').modal('show');
    $('body').removeClass('modal-open');
    if (res.status === 1 && res.data != null) {
      this.searchUserError = false;
      this.searchUserData = res.data;
    } else {
      this.searchUserError = true;
      this.searchUserData = undefined;
    }
  }

  doStatusUpdate(uid, typ) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if (typ === 1) {
          this.service3.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        } else {
          this.service3.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        }
      }
    });
  }

  doStatusDelete(uid) {
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service3.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.submitForm();
    }
  }

}



import { Component, OnInit } from '@angular/core';

declare var $;
@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.css']
})
export class RightSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onCloseRightMenu(e) {
    e.preventDefault();
    $('body').toggleClass('right-bar-enabled');
  }




}

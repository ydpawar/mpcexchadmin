import { Injectable } from '@angular/core';
import * as $ from 'jquery';

declare let document: any;

interface Script {
  src: string;
  loaded: boolean;
}

@Injectable()
export class ScriptLoaderService {
  private _scripts: Script[] = [];
  private tag: any;
  private fileref: any;

  load(tag, ...scripts: string[]) {
    this.tag = tag;
    scripts.forEach((script: string) => this._scripts[script] = { src: script, loaded: false });

    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script, 'assets/js/foo-tables.init.js')));
    return Promise.all(promises);
  }

  loadScript(src: string, js: string) {
    return new Promise((resolve, reject) => {
// debugger;
      // resolve if already loaded
      if (this._scripts[src].loaded) {
        resolve({ script: src, loaded: true, status: 'Already Loaded' });
      } else {
        // load script
        const script = $('<script/>')
          .attr('type', 'text/javascript')
          .attr('src', this._scripts[src].src);

        $(this.tag).append(script);
        resolve({ script: src, loaded: true, status: 'Loaded' });
      }
    });
  }

  /* replacejscssfile(oldfilename, newfilename, filetype) {
    //debugger;
    const targetelement = (filetype === 'js') ? 'script' : (filetype === 'css') ? 'link' : 'none';
    const targetattr = (filetype === 'js') ? 'src' : (filetype === 'css') ? 'href' : 'none';
    const allsuspects = document.getElementsByTagName(targetelement);
    for (let i = allsuspects.length; i >= 0; i--) { // search backwards within nodelist for matching elements to remove
      // tslint:disable-next-line:max-line-length
      if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) !== null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename) !== -1) {
        const newelement = this.createjscssfile(newfilename, filetype);
        allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i]);
      }
    }
  }

  createjscssfile(filename, filetype) {
    if (filetype === 'js') {
      const fileref = document.createElement('script');
      fileref.setAttribute('type', 'text/javascript');
      fileref.setAttribute('src', filename);
    } else if (filetype === 'css') { // if filename is an external CSS file
      const fileref = document.createElement('link');
      fileref.setAttribute('rel', 'stylesheet');
      fileref.setAttribute('type', 'text/css');
      fileref.setAttribute('href', filename);
    }
    return this.fileref;
  }*/
}

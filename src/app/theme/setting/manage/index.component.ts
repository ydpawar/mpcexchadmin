import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { SettingService } from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
interface Setting {
  id: string;
  key: string;
  value: string;
  status: {
    val: string;
    text: string;
    class1: string;
    class2: string;
  };
  form_type: string;
  value_array: any;
  fieldKey :any;
}

class SettingObj implements Setting {
  id: string;
  key: string;
  value: string;
  status: {
    val: string;
    text: string;
    class1: string;
    class2: string;
  };
  form_type: string;
  value_array: any;
  fieldKey :any;
}

@Component({
  selector: 'app-manage',
  templateUrl: './index.component.html',
  providers: [SettingService]
})
export class ManageComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  frmSetting: FormGroup;
  page = { start: 1, end: 5 };

  aList: Setting[] = [];
  cItem: Setting;
  keyName: string;
  fieldKey :any;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_setting';

  title = 'Setting';
  createUrl = '/setting/create';
  breadcrumb: any = [{title: 'Setting', url: '/' }, {title: 'Manage', url: '/' }];

  constructor(
      private formBuilder: FormBuilder,
      private service: SettingService,
      private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_setting').dataTable().fnDestroy();
        const items = res.data;
        const data: Setting[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: Setting = new SettingObj();

            cData.id = item.id;
            cData.key = item.key_name;
            cData.value = item.value;
            cData.form_type = item.form_type;
            cData.value_array = item.value_array;
            cData.fieldKey   = item.fieldKey;
            cData.status = {
              val: item.status,
              text: item.status === 1 ? 'Active' : 'Inactive',
              class1: item.status === 1 ? 'success' : 'red',
              class2: item.status === 1 ? 'fa-check' : 'fa-times'
            };

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
    this.spinner.hide();
  }

  changeStatus(item) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        // tslint:disable-next-line:triple-equals
        const data = { id: item.id, status: ( item.status.val == 1 ? 0 : 1 )};
        this.service.status(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  updateSetting(item) {
    this.keyName = item.key;
    this.fieldKey  = item.fieldKey;

    if (item.form_type == 0) {
      this.frm.reset();
      $('.clear-settlement').modal('show');
      $('body').removeClass('modal-open');
      this.frm.patchValue({
        value: item.value,
        key_name: item.key,
        id: item.id,
        form_type: item.form_type
      });
    }

    if (item.form_type == 1) {
      this.frmSetting.reset();
      $('.modal-msetting').modal('show');
      $('body').removeClass('modal-open');
      this.frmSetting.patchValue({
        key_name: item.key,
        id: item.id,
        form_type: item.form_type
      })
      this.frmSetting.patchValue(item.value_array);
    }


  }

  createForm() {
    this.frm = this.formBuilder.group({
      value: ['', Validators.required],
      key_name: [''],
      id: ['', Validators.required],
      form_type: ['']
    });

    this.frmSetting = this.formBuilder.group({
      id: ['', Validators.required],
      form_type: [''],
      key_name: [''],
      Max_stack: ['', Validators.required],
      Min_stack: ['', Validators.required],
      Max_profit_limit: [''],
      Event_Max_profit_all_limit: [''],
      Max_odd_limit: [''],
      Max_profit_all_limit:[''],
      Bet_delay: ['', Validators.required]
    })
  }


  get frmValue() { return this.frm.get('value'); }

  get maxStack() { return this.frmSetting.get('Max_stack'); }
  get minStack() { return this.frmSetting.get('Min_stack'); }
  get maxProfitLmt() { return this.frmSetting.get('Max_profit_limit'); }
  get maxProfitAllLmt() { return this.frmSetting.get('Max_profit_all_limit'); }
  get eventMaxProfitAllLimit() { return this.frmSetting.get('Event_Max_profit_all_limit'); }
  get maxOddLmt() { return this.frmSetting.get('Max_odd_limit'); }
  get betDelay() { return this.frmSetting.get('Bet_delay'); }

  submitForm() {
    if(this.frm.value.form_type == 0){
    if (this.frm.valid) {
      const data = this.frm.value;
      this.service.update(data).subscribe((res) => this.onSubmit(res));
    }}
    if(this.frmSetting.value.form_type == 1){
      const data = this.frmSetting.value;
      this.service.update(data).subscribe((res) => this.onSubmitSetting(res));
    }
  }
  
  onSubmit(res) {
    if (res.status === 1) {
      document.getElementById('close-settlement').click();
      this.applyFilters();
    }
  }

  onSubmitSetting(res) {
    if (res.status === 1) {
      $('.modal-msetting').modal('hide');
      this.frmSetting.reset();
      this.applyFilters();
    }
  }


  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_setting').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }


}

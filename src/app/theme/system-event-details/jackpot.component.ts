import {Component, OnDestroy, OnInit} from '@angular/core';
import { DashboardService} from '../../_api';
import {ActivatedRoute} from '@angular/router';
import {AuthIdentityService, ToastrService} from '../../_services';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './jackpot.component.html',
  styleUrls: ['./index.component.css'],
  providers: [DashboardService]
})

export class SystemJackpotDetailsComponent implements OnInit , OnDestroy {

  title = 'System Jackpot Details';
  breadcrumb: any = [{title: 'System Jackpot Details', url: '/' }];

  uid: string;
  eid: string;
  type: string;
  mid: string;
  sport: string;
  isEmpty = true;
  eventData: any = [];
  jackpotData: any = [];
  marketIdsArr: any = [];
  betList: any = [];
  cUserData: any;

  private isDestroy: boolean = false;
  private isFirstLoad: boolean = false;
  // tslint:disable-next-line:max-line-length
  constructor(
      private service: DashboardService,
      private route: ActivatedRoute,
      private authIdentity: AuthIdentityService,
      private spinner: NgxSpinnerService,
      private toaster: ToastrService) {

      this.type = this.route.snapshot.params.type;
      this.eid = this.route.snapshot.params.eid;
      this.mid = this.route.snapshot.params.mid;
      this.uid = this.route.snapshot.params.uid;

  }

  ngOnInit() {
    this.spinner.show();
    const data = {type: this.type, eid: this.eid, mid: this.mid, uid: this.uid};
    this.getJackpotDetail(data);

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.cUserData = auth.getIdentity();
    }

  }

  ngOnDestroy(): void {
    this.isDestroy = true;
  }

  async getJackpotDetail(data) {
    await this.service.getSystemJackpotDetail(data).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccessDataList(data);
        },
        error => {
          // this.toaster.error('Error in Get JackpotList Api !!', 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.isEmpty = true;
        if ( response.data.items !== undefined ) {
          if ( response.data.items ) {
            this.jackpotData = response.data.items;
          }
          if ( response.data.betList && response.data.betList.count > 0 ) {
            this.betList = response.data.betList;
          }
        }

        if (!this.isDestroy) {
          const data = {type: this.type, eid: this.eid, mid: this.mid, uid: this.uid};
          const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getJackpotDetail(data); }, 5000);
        }
      }
    }
    if ( !this.isFirstLoad ) {
      this.isFirstLoad = true;
      this.spinner.hide();
    }
  }

  getLastChar(id: any) {
    return id.substr(id.length - 6);
  }

}

import {Component, OnInit, OnDestroy} from '@angular/core';
import {DashboardService} from '../../_api';
import {ActivatedRoute} from '@angular/router';
import {AuthIdentityService, ToastrService} from '../../_services';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [DashboardService]
})

export class SystemEventDetailsComponent implements OnInit , OnDestroy {

    title = 'System Event Details';
    breadcrumb: any = [{title: 'System Event Details', url: '/'}];

    uid: string;
    eid: string;
    sport: string;
    marketName: string;
    isEmpty = true;
    eventData: any = [];
    otherMarket: any = [];
    winner: any = null;
    matchOdd: any = null;
    completedMatch: any = null;
    tiedMatch: any = null;
    bookMaker: any = null;
    goalsMaker: any = null;
    setMarket: any = null;
    virtualCricket: any = null;
    fancyMarket: any = null;
    fancy2Market: any = null;
    meterFancy: any = null;
    fancy3Market: any = null;
    oddEvenMarket: any = null;
    khadoSession: any = null;
    ballSession: any = null;
    jackpotData: any = null;
    cricketCasino: any = null;
    binaryMarket: any = null;
    marketIdsArr: any = [];
    marketOdds: any = [];
    betList1: any = [];
    betList2: any = [];
    betList3: any = [];
    betList4: any = [];
    betList5: any = [];
    betList6: any = [];
    marketIds: any = [];

    bookData: any  = null;
    bookDataFancy: any = null;
    bookDataCasino: any = null;
    cUserData: any;
    private isDestroy: boolean = false;
    private isFirstLoad: boolean = false;

    tvAllowed = false;
    xhm5000: number;
    xhm3000: number;
    xhm1000: number;
    tvUrl: any;

    // tslint:disable-next-line:max-line-length
    constructor(
        public sanitizer: DomSanitizer,
        private service: DashboardService,
        private route: ActivatedRoute,
        private authIdentity: AuthIdentityService,
        private spinner: NgxSpinnerService,
        private toaster: ToastrService) {
        this.eid = this.route.snapshot.params.eid;
        this.sport = this.route.snapshot.params.sport;
        this.uid = this.route.snapshot.params.uid;
        // this.tvUrl += this.eid;
        const tt = 'https://dreamexch9.co.in/new-tvscreen.php?mid=' + this.eid;
        this.tvUrl =  sanitizer.bypassSecurityTrustResourceUrl(tt);
    }

    ngOnInit() {
        this.spinner.show();
        this.getDetail(this.eid, this.sport);
        this.getBetList(this.eid);
    }

    ngOnDestroy(): void {
        clearTimeout(this.xhm5000);
        clearTimeout(this.xhm3000);
        clearTimeout(this.xhm1000);
        this.isDestroy = true;
    }

    async getDetail(eid, sport) {
        await this.service.getSystemDetailNew(eid).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessDataList(data);
            },
            error => {
                // this.toaster.error('Error in Get Detail Api !!', 'Something want wrong..!');
            });
    }

    onSuccessDataList(response) {
        if (response.status !== undefined) {
            if (response.status === 1) {
                this.isEmpty = true;
                if (response.data.items !== undefined) {

                    this.eventData = response.data.items;
                    this.cUserData = response.data.userData;

                    // Winner
                    if (response.data.items.otherMarket && response.data.items.otherMarket.Winner) {
                        this.winner = response.data.items.otherMarket.Winner;
                    } else { this.winner = null; }
                    // MatchOdd
                    if (response.data.items.otherMarket && response.data.items.otherMarket.MatchOdd) {
                        this.matchOdd = response.data.items.otherMarket.MatchOdd;
                    } else { this.matchOdd = null; }
                    // CompletedMatch
                    if (response.data.items.otherMarket && response.data.items.otherMarket.CompletedMatch) {
                        this.completedMatch = response.data.items.otherMarket.CompletedMatch;
                    } else { this.completedMatch = null; }
                    // TiedMatch
                    if (response.data.items.otherMarket && response.data.items.otherMarket.TiedMatch) {
                        this.tiedMatch = response.data.items.otherMarket.TiedMatch;
                    } else { this.tiedMatch = null; }

                    // goalsMaker
                    if (response.data.items.goalsMaker && response.data.items.goalsMaker.length > 0) {
                        this.goalsMaker = response.data.items.goalsMaker;
                    } else { this.goalsMaker = null; }

                    // setMarket
                    if (response.data.items.setMarket && response.data.items.setMarket.length > 0) {
                        this.setMarket = response.data.items.setMarket;
                    } else { this.setMarket = null; }

                    // bookMaker
                    if (response.data.items.bookMaker && response.data.items.bookMaker.length > 0) {
                        this.bookMaker = response.data.items.bookMaker;
                    } else { this.bookMaker = null; }

                    // virtualCricket
                    if (response.data.items.virtualCricket && response.data.items.virtualCricket.length > 0) {
                        this.virtualCricket = response.data.items.virtualCricket;
                    } else { this.virtualCricket = null; }

                    // FancyMarket
                    if (response.data.items.fancyMarket && response.data.items.fancyMarket.length > 0) {
                        this.fancyMarket = response.data.items.fancyMarket;
                    } else { this.fancyMarket = null; }
                    // Fancy2Market
                    if (response.data.items.fancy2Market && response.data.items.fancy2Market.length > 0) {
                        this.fancy2Market = response.data.items.fancy2Market;
                    } else { this.fancy2Market = null; }
                    // MeterFancy
                    if (response.data.items.meterFancy && response.data.items.meterFancy.length > 0) {
                        this.meterFancy = response.data.items.meterFancy;
                    } else { this.meterFancy = null; }
                    // Fancy3Market
                    if (response.data.items.fancy3Market && response.data.items.fancy3Market.length > 0) {
                        this.fancy3Market = response.data.items.fancy3Market;
                    } else { this.fancy3Market = null; }
                    // OddEvenMarket
                    if (response.data.items.oddEvenMarket && response.data.items.oddEvenMarket.length > 0) {
                        this.oddEvenMarket = response.data.items.oddEvenMarket;
                    } else { this.oddEvenMarket = null; }
                    // KhadoSession
                    if (response.data.items.khadoSession && response.data.items.khadoSession.length > 0) {
                        this.khadoSession = response.data.items.khadoSession;
                    } else { this.khadoSession = null; }
                    // BallSession
                    if (response.data.items.ballSession && response.data.items.ballSession.length > 0) {
                        this.ballSession = response.data.items.ballSession;
                    } else { this.ballSession = null; }
                    // JackpotData
                    if (response.data.items.jackpotData && response.data.items.jackpotData.length > 0) {
                        this.jackpotData = response.data.items.jackpotData;
                    } else { this.jackpotData = null; }
                    // cricketCasino
                    if (response.data.items.cricketCasino && response.data.items.cricketCasino.length > 0) {
                        this.cricketCasino = response.data.items.cricketCasino;
                    } else { this.cricketCasino = null; }

                    // BinaryMarket
                    if (response.data.items.binaryMarket && response.data.items.binaryMarket.length > 0) {
                        this.binaryMarket = response.data.items.binaryMarket;
                    } else { this.binaryMarket = null; }

                    this.marketIdsArr = response.data.marketIdsArr;
                    if (this.marketIdsArr && this.marketIdsArr.length > 0 && !this.isFirstLoad) {
                        // this.isFirstLoad = true;
                        this.getDataOdds(this.marketIdsArr);
                    }
                }

                if (this.eventData.tvAllowed === 1) {
                    this.tvAllowed = true;
                } else {
                    this.tvAllowed = false;
                }

                // tslint:disable-next-line:triple-equals
                if (!this.isDestroy && this.sport != 'live-games') {
                    this.xhm5000 = setTimeout(() => { clearTimeout(this.xhm5000); this.getDetail(this.eid, this.sport); }, 5000);
                }
            }
        }
        if (!this.isFirstLoad) {
            this.isFirstLoad = true;
            this.spinner.hide();
        }
    }

    async getBetList(eid) {
        await this.service.getSystemBetList(eid).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessBetList(data);
            },
            error => {
                // this.toaster.error('Error in Get BetList Api !!', 'Something want wrong..!');
            });
    }

    onSuccessBetList(response) {
        if ( response.status === 1) {
            if (response.data !== undefined) {
                this.betList1 = response.data.betList1;
                this.betList2 = response.data.betList2;
                this.betList3 = response.data.betList3;
                this.betList4 = response.data.betList4;
                this.betList5 = response.data.betList5;
                this.betList6 = response.data.betList6;
            }

            if (response.marketIds !== undefined) {
                this.marketIds = response.marketIds;
            }

            if (!this.isDestroy) {
                this.xhm3000 = setTimeout(() => { clearTimeout(this.xhm3000); this.getBetList(this.eid); }, 3000);
            }
        }
    }

    checkMarket(mid) {
        const isLargeNumber = (element) => element === mid;
        if ( this.marketIds.findIndex(isLargeNumber) >= 0 ) {
            return true;
        } else {
            return false;
        }
    }

    checkAllowedSports(type, sid) {
        let allowedSports = [];
        if ( type === 'bookmaker' || type === 'betlist1' ) {
            allowedSports = [1,2,4,6,13,14,16,22,3503,7522,998917];
        } else if ( type === 'meter' || type === 'fancy2' || type === 'fancy3' || type === 'betlist2' ) {
            allowedSports = [1,2,4,6,10,13,14,16,22,3503,7522,998917];
        } else if ( type === 'matchodd' ) {
            allowedSports = [1,2,4,16,3503,7522,998917];
        } else if ( type === 'set_market' || type === 'goals' || type === 'betlist6' ) {
            allowedSports = [1,2];
        }
        const isLargeNumber = (element) => element === sid;
        if (allowedSports.findIndex(isLargeNumber) >= 0) { return true; } else { return false; }
    }

    async getDataOdds(data) {
        await this.service.getOdds(data).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessDataOdds(data);
            },
            error => {
                // if (!this.isDestroy) {
                //     const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataOdds(this.marketIdsArr); }, 3000);
                // }
                // this.toaster.error('Error in Get Odds Api !!', 'Something want wrong..!');
            });
    }

    onSuccessDataOdds(response) {
        if (response.status !== undefined && response.status === 1) {
            if (response.data !== undefined && response.data.items) {
                this.marketOdds = response.data.items;
            }
            if (!this.isDestroy) {
                this.xhm1000 = setTimeout(() => { clearTimeout(this.xhm1000); this.getDataOdds(this.marketIdsArr); }, 2000);
            }
        }
        // else {
        //     if (!this.isDestroy) {
        //         const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataOdds(this.marketIdsArr); }, 3000);
        //     }
        // }
    }

    doBetAllowed(eid) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doBetAllowed(eid).subscribe((res) => this.onSuccessBetAllowed(res));
            }
        });
    }

    doBetAllowedMarket(mid, type) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doBetAllowedMarket(mid, type).subscribe((res) => this.onSuccessBetAllowed(res));
            }
        });
    }

    onSuccessBetAllowed(res) {
        if (res.status === 1) {
            this.isFirstLoad = false;
            clearTimeout(this.xhm5000);
            clearTimeout(this.xhm3000);
            clearTimeout(this.xhm1000);
            this.getDetail(this.eid, this.sport);
            this.getBetList(this.eid);
        }
    }

    getBookDataMatchOdd(uid, mid, marketName) {
        // this.bookData = [];
        this.marketName = marketName;
        const data = {userId: uid, marketId: mid};
        this.service.getSystemBookDataMatchOdd(data).subscribe((res) => this.onSuccessBookDataMatchOdd(res));
    }

    onSuccessBookDataMatchOdd(res) {
        if (res.status === 1) {
            this.bookData = res.data;
        }
    }

    getBookDataBookMaker(uid, mid, marketName) {
        // this.bookData = [];
        this.marketName = marketName;
        const data = {userId: uid, marketId: mid};
        this.service.getSystemBookDataBookMaker(data).subscribe((res) => this.onSuccessBookDataBookMaker(res));
    }

    onSuccessBookDataBookMaker(res) {
        if (res.status === 1) {
            this.bookData = res.data;
        }
    }

    /*getBookDataFancy(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataFancy(data).subscribe((res) => this.onSuccessBookDataFancy(res));
        }
    }

    onSuccessBookDataFancy(res) {
        if (res.status === 1) {
            this.bookDataFancy = res.data;
        }
        this.spinner.hide();
    }*/

    // new book api
    getBookDataBinary(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataBinary(data).subscribe((res) => this.onSuccessBookDataFancy(res));
        }
    }

    // new book api
    getBookDataFancy(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataFancy(data).subscribe((res) => this.onSuccessBookDataFancy(res));
        }
    }

    onSuccessBookDataFancy(res) {
        if (res.status === 1) {
            let i = 0;
            let min = res.data.min; let max = res.data.max;
            let dataReturn = [];
            for ( i = min; i <= max; i++ ) {
                let betlist = res.data.betList;
                let total = 0; let winVal1 = 0; let winVal2 = 0; let lossVal1 = 0; let lossVal2 = 0;
                if ( betlist !== undefined && betlist != null ) {
                    // tslint:disable-next-line:only-arrow-functions
                    betlist.forEach(function(bets) {
                        if (bets.bType === 'no' && bets.price > i) {
                            winVal1 = winVal1 + (((bets.win) * (bets.apl) / 100));
                        } else if (bets.bType === 'yes' && bets.price <= i) {
                            winVal2 = winVal2 + (((bets.win) * (bets.apl) / 100));
                        } else if (bets.bType === 'no' && bets.price <= i) {
                            lossVal1 = lossVal1 + (((bets.loss) * (bets.apl) / 100));
                        } else if (bets.bType === 'yes' && bets.price > i) {
                            lossVal2 = lossVal2 + (((bets.loss) * (bets.apl) / 100));
                        }
                    });
                    total = (lossVal1 + lossVal2) - (winVal1 + winVal2);
                    dataReturn.push( { 'price' : i , 'profitLoss' : Math.round(total) } );
                }

            }

            const bookDataFancyNew = [];
            if ( dataReturn.length) {
                let priceVal;
                let i = 0;
                let start = 0;
                let startPl = 0;
                let end = 0;

                // tslint:disable-next-line:only-arrow-functions
                dataReturn.forEach(function(d, index) {
                    if (index === 0) {
                        bookDataFancyNew.push( { 'price' : d['price'] + ' or less', 'profitLoss' : d['profitLoss'] } );
                    } else {
                        if (startPl !== d['profitLoss']) {
                            if (end !== 0) {
                                if (start === end) {
                                    priceVal = start;
                                } else {
                                    priceVal = start + ' - ' + end;
                                }
                                bookDataFancyNew.push( { 'price' : priceVal, 'profitLoss' : startPl } );
                            }

                            start = d['price'];
                            end = d['price'];

                        } else {
                            end = d['price'];
                        }

                        if (index === (dataReturn.length - 1)) {
                            bookDataFancyNew.push( { 'price' : start + ' or more', 'profitLoss' : startPl } );
                        }

                    }

                    startPl = d['profitLoss'];
                    i++;
                });
            }

            this.bookDataFancy = bookDataFancyNew;
        }
        this.spinner.hide();
    }

    getBookDataFancy3(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataFancy3(data).subscribe((res) => this.onSuccessBookDataFancy3(res));
        }
    }

    onSuccessBookDataFancy3(res) {
        if (res.status === 1) {
            // let i = 0;
            // let min = res.data.min; let max = res.data.max;
            let dataReturn = [];
            // for ( i = min; i <= max; i++ ) {
            let betlist = res.data.betList;
            let yesPl = 0; let noPl = 0; let yesWinVal = 0; let yesLossVal = 0; let noWinVal = 0; let noLossVal = 0;
            if ( betlist !== undefined && betlist != null ) {
                // tslint:disable-next-line:only-arrow-functions
                betlist.forEach( function(bets) {
                    if ( bets.bType === 'back' ) {
                        yesWinVal = yesWinVal + ( ( ( bets.win ) * ( bets.apl ) / 100 ) );
                        yesLossVal = yesLossVal + ( ( ( bets.loss ) * ( bets.apl ) / 100 ) );
                    } else if ( bets.bType === 'lay' ) {
                        noWinVal = noWinVal + ( ( ( bets.win ) * ( bets.apl ) / 100 ) );
                        noLossVal = noLossVal + ( ( ( bets.loss ) * ( bets.apl ) / 100 ) );
                    }
                });
                yesPl = (-1) * ( yesWinVal - noLossVal );
                noPl = (-1) * ( noWinVal - yesLossVal );
            }

            dataReturn['yesPl'] = Math.round(yesPl);
            dataReturn['noPl'] = Math.round(noPl);
            // }
            this.bookDataFancy = dataReturn;
        }
        this.spinner.hide();
    }

    getBookDataBallSession(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataBallSession(data).subscribe((res) => this.onSuccessBookDataBallSession(res));
        }
    }

    onSuccessBookDataBallSession(res) {
        if (res.status === 1) {
            let i = 0;
            let min = res.data.min; let max = res.data.max;
            let dataReturn = [];
            for ( i = min; i <= max; i++ ) {
                let betlist = res.data.betList;
                let total = 0; let winVal1 = 0; let winVal2 = 0; let lossVal1 = 0; let lossVal2 = 0;
                if ( betlist !== undefined && betlist != null ) {
                    // tslint:disable-next-line:only-arrow-functions
                    betlist.forEach(function(bets) {
                        if (bets.bType === 'no' && bets.price > i) {
                            winVal1 = winVal1 + (((bets.win) * (bets.apl) / 100));
                        } else if (bets.bType === 'yes' && bets.price <= i) {
                            winVal2 = winVal2 + (((bets.win) * (bets.apl) / 100));
                        } else if (bets.bType === 'no' && bets.price <= i) {
                            lossVal1 = lossVal1 + (((bets.loss) * (bets.apl) / 100));
                        } else if (bets.bType === 'yes' && bets.price > i) {
                            lossVal2 = lossVal2 + (((bets.loss) * (bets.apl) / 100));
                        }
                    });
                    total = (lossVal1 + lossVal2) - (winVal1 + winVal2);
                    dataReturn.push( { 'price' : i , 'profitLoss' : Math.round(total) } );
                }

            }

            const bookDataFancyNew = [];
            if ( dataReturn.length ) {
                let priceVal;
                let i = 0;
                let start = 0;
                let startPl = 0;
                let end = 0;

                // tslint:disable-next-line:only-arrow-functions
                dataReturn.forEach(function(d, index) {
                    if (index === 0) {
                        bookDataFancyNew.push( { 'price' : d['price'] + ' or less', 'profitLoss' : d['profitLoss'] } );
                    } else {
                        if (startPl !== d['profitLoss']) {
                            if (end !== 0) {
                                if (start === end) {
                                    priceVal = start;
                                } else {
                                    priceVal = start + ' - ' + end;
                                }
                                bookDataFancyNew.push( { 'price' : priceVal, 'profitLoss' : startPl } );
                            }

                            start = d['price'];
                            end = d['price'];

                        } else {
                            end = d['price'];
                        }

                        if (index === (dataReturn.length - 1)) {
                            bookDataFancyNew.push( { 'price' : start + ' or more', 'profitLoss' : startPl } );
                        }

                    }

                    startPl = d['profitLoss'];
                    i++;
                });
            }

            this.bookDataFancy = bookDataFancyNew;
        }
        this.spinner.hide();
    }

    getBookDataKhado(uid, mid, marketName) {
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataKhado(data).subscribe((res) => this.onSuccessBookDataKhado(res));
        }
    }

    onSuccessBookDataKhado(res) {
        if (res.status === 1) {
            let i = 0;
            let min = res.data.min; let max = res.data.max;
            let dataReturn = [];
            for ( i = min; i <= max; i++ ) {
                let betlist = res.data.betList;
                let total = 0; let winVal = 0; let lossVal = 0;
                if ( betlist !== undefined && betlist != null ) {
                    // tslint:disable-next-line:only-arrow-functions
                    betlist.forEach(function(bets) {
                        if (bets.diff > i && bets.price <= i) {
                            winVal = winVal + (((bets.win) * (bets.apl) / 100));
                        } else if (bets.diff <= i || bets.price > i) {
                            lossVal = lossVal + (((bets.loss) * (bets.apl) / 100));
                        }
                    });
                    total = lossVal - winVal;
                    dataReturn.push( { 'price' : i , 'profitLoss' : Math.round(total) } );
                }

            }
            this.bookDataFancy = dataReturn;
        }
        this.spinner.hide();
    }

    getBookDataCasino(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.spinner.show();
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getSystemBookDataCasino(data).subscribe((res) => this.onSuccessBookDataCasino(res));
        }
    }

    onSuccessBookDataCasino(res) {
        if (res.status === 1) {
            this.bookDataCasino = res.data;
        }
        this.spinner.hide();
    }

    getLastChar(id: any) {
        return id.substr(id.length - 6);
    }

}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { ReportsComponent } from './index.component';
import {ExcelService} from '../../common/excel.service';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: 'account-statement',
        loadChildren: './account-statement/index.module#AccountStatementModule'
      },
      {
        path: 'account-statement-month',
        loadChildren: './account-statement-month/index.module#AccountStatementMonthModule'
      },
      {
        path: 'profit-loss',
        loadChildren: './profit-loss/index.module#ProfitLossModule'
      },
      {
        path: 'system-profit-loss',
        loadChildren: './system-profit-loss/index.module#SystemProfitLossModule'
      },
      {
        path: 'casino-profit-loss',
        loadChildren: './casino-profit-loss/index.module#CasinoProfitLossModule'
      },
      {
        path: 'system-casino-profit-loss',
        loadChildren: './system-casino-profit-loss/index.module#SystemCasinoProfitLossModule'
      },
      {
        path: 'teenpatti-profit-loss',
        loadChildren: './teenpatti-profit-loss/index.module#TeenpattiProfitLossModule'
      },
      {
        path: 'system-teenpatti-profit-loss',
        loadChildren: './system-teenpatti-profit-loss/index.module#SystemTeenpattiProfitLossModule'
      },
      {
        path: 'live-game-profit-loss',
        loadChildren: './teenpatti-profit-loss/index.module#TeenpattiProfitLossModule'
      },
      {
        path: 'live-game2-profit-loss',
        loadChildren: './teenpatti-profit-loss/index.module#TeenpattiProfitLossModule'
      },
      {
        path: 'system-live-game-profit-loss',
        loadChildren: './system-teenpatti-profit-loss/index.module#SystemTeenpattiProfitLossModule'
      },
      {
        path: 'system-live-game2-profit-loss',
        loadChildren: './system-teenpatti-profit-loss/index.module#SystemTeenpattiProfitLossModule'
      },
      {
        path: 'chip-history',
        loadChildren: './chip-history/index.module#ChipHistoryModule'
      },
      {
        path: 'activity-log',
        loadChildren: './activity-log/index.module#ActivityLogModule'
      },
      {
        path: 'market-bet',
        loadChildren: './market-bet/index.module#MarketBetModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [ReportsComponent],
  providers: [ExcelService]
})
export class ReportsModule {
}

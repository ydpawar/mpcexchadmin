import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {BaseComponent} from '../../../../common/commonComponent';
import {FormGroup, Validators} from '@angular/forms';
import {AuthIdentityService} from '../../../../_services';
import * as moment from 'moment'; // Momentjs
declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  title = 'Casino Profit Loss';
  breadcrumb: any = [{title: 'Casino Profit Loss', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };
  user: any;
  uid = '';
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  isDefault = true;
  dataTableId = 'DataTables_casino_pl';

  public filter: any ={};
  fType :string ='';
  constructor(
      inj: Injector,
      private service: HistoryService,
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    // this.spinner.show();
    this.createForm();
    // this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    const data = {uid: this.uid}; this.isDefault = false;
    this.service.casinoProfitLoss(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_casino_pl').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        this.title = 'Casino Profit Loss for ' + res.userName;
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.sportId = item.sportId;
            cData.eventId = item.eventId;
            cData.pl = item.profitLoss;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        if (res.total) {
          this.aListTotal = res.total;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_casino_pl');
      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      uid: [this.uid],
    });
  }

  submitForm( fType = null ) {
    this.isDefault = false;
    if ( fType != null ) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType;
      this.service.casinoProfitLoss(data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.casinoProfitLoss(data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      this.onSuccess(res);
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
      const data={'start': sDate ,'end' : eDate}
      this.frm.patchValue(data);
      this.submitForm(this.fType);
  }
}


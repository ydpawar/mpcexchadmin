import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MarketBetComponent } from './index.component';

const routes: Routes = [
  {
    path: ':mid',
    component: MarketBetComponent,
  },
  {
    path: ':mid/:uid',
    component: MarketBetComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    MarketBetComponent
  ]
})
export class MarketBetModule {
}

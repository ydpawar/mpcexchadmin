import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SettlementLiveComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: SettlementLiveComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SettlementLiveComponent
  ]
})
export class SettlementLiveModule {
}

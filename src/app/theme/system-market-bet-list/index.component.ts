import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {HistoryService} from '../../_api';
import {ScriptLoaderService} from '../../_services';
import {ActivatedRoute} from '@angular/router';
import swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: object;
  runner: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: object;
  runner: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class SystemMarketBetListComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  title = 'System Market Bet List';
  breadcrumb: any = [{title: 'System Market Bet List', url: '/' }];

  page = { start: 1, end: 5 };

  mtype: string;
  mid: string;
  eid: string;
  totalCount: string = '0';
  loadCount: string = '0';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  isLoadMore = true;
  cUserData: any;
  betIdsArr: any = [];
  dataTableId = 'DataTables_market_bet';
  private isDestroy: boolean = false;
  private isFirstLoad: boolean = false;

  public notscrolly: boolean = true;
  public notEmptyPost: boolean = true;
  public pageNo: number = 1;

  constructor(
      private formBuilder: FormBuilder,
      private service: HistoryService,
      private loadJs: ScriptLoaderService,
      private spinner: NgxSpinnerService,
      private route: ActivatedRoute
  ) {
    this.mtype = this.route.snapshot.params.mtype;
    this.mid = this.route.snapshot.params.mid;
    this.eid = this.route.snapshot.params.eid;
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
    this.isDestroy = true;
  }

  ngAfterViewInit() {
  }

  onScroll() {
    if (this.notEmptyPost && this.isLoadMore && this.notscrolly) {
      this.notscrolly = false;
      this.pageNo++;
      this.applyFilters();
    }
  }

  applyFilters() {
    const data = { mtype: this.mtype, mid: this.mid, eid: this.eid, page: this.pageNo};
    this.service.systemMarketBets(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      // $('#DataTables_market_bet').dataTable().fnDestroy();
      this.cUserData = res.userData;

      if ( res.userData.name !== undefined ) {
        this.title = 'System Market Bet List for ' + res.userData.name;
      }

      if (res.data !== undefined ) {
        const items = res.data.list;
        const data: listData[] = [];

        if ( res.data.count > 0) {
          // this.aList = [];
          this.loadCount = res.data.loadCount;
          this.totalCount = res.data.count;
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item._id;
            cData.runner = item.runner;
            cData.client = item.client;
            cData.price = item.price;
            // tslint:disable-next-line:max-line-length
            if ( item.mType === 'set_market' || item.mType === 'goals' || item.mType === 'match_odd' || item.mType === 'bookmaker' || item.mType === 'fancy3' ) {
              cData.rate = '-';
            } else {
              cData.rate = item.rate;
            }
            cData.stake = item.size;
            if ( item.bType === 'back' || item.bType === 'yes') {
              cData.pl = item.win;
            } else {
              cData.pl = item.loss;
            }
            if (item.mType === 'meter' ) {
              cData.pl = '-';
            }
            cData.ip = item.ip_address;
            cData.master = item.master;
            cData.time = item.created_on;
            cData.bType = item.bType;

            // data.push(cData);
            this.aList.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        // this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadScript();

      }

      // if (!this.isDestroy) {
      //   const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.applyFilters(); }, 5000);
      // }
      this.notscrolly = true;
    }
    this.spinner.hide();
  }

  doBetDelete() {
    swal.fire({
      title: 'Are you sure to want delete this all bets ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      input: 'password',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Activity Password',
        type: 'text',
        pattern: '^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})',
      },
      validationMessage: 'invalid password !',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        const data = {betIds: this.betIdsArr, password: result.value};
        this.service.doBetDelete(data).subscribe((res) => this.onSuccessBetDelete(res));
      }
    });
  }

  onSuccessBetDelete(res) {
    if (res.status === 1) {
      this.aList = []; this.pageNo = 1;
      this.betIdsArr = [];
      this.applyFilters();
    }
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_market_bet').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  createForm() {
    this.frm = this.formBuilder.group({
      client: [''],
      runner: [''],
    });
  }

  pushDelete(betId) {
    if ( this.checkBetId(betId) ) {
      const index: number = this.betIdsArr.indexOf(betId);
      if (index !== -1) {
        this.betIdsArr.splice(index, 1);
      }
    } else {
      this.betIdsArr.push(betId);
    }
  }

  checkBetId(betId) {
    const isLargeNumber = (element) => element === betId;
    if (this.betIdsArr && this.betIdsArr.findIndex(isLargeNumber) >= 0) {
      return true;
    } else {
      return false;
    }
  }

  submitForm() {
    // if (this.frm.valid) {
      const data = this.frm.value;
      data.mtype = this.mtype; data.mid = this.mid; data.eid = this.eid;
      this.service.systemMarketBets(data).subscribe((res) => this.onSearch(res));
    // }
  }

  onSearch(res) {
    if (res.status === 1) {
      this.aList = [];
      this.spinner.hide();
      this.isLoadMore = false;
      this.onSuccess(res);
    }
  }

  get frmClient() { return this.frm.get('client'); }
  get frmRunner() { return this.frm.get('runner'); }

  getLastChar(id: any) {
    return id.substr(id.length - 6);
  }

}

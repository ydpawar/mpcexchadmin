import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SuperAdminService } from '../../../../_api/index';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SuperAdminService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'Super Admin - Create';
  breadcrumb: any = [{title: 'Super Admin', url: '/' }, {title: 'Create', url: '/' }];

  frm: FormGroup;
  userError: string;
  parentUser: string;

  constructor(
      private formBuilder: FormBuilder,
      private service: SuperAdminService,
      private router: Router,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngAfterViewInit() {
  }

  validatePassword(control: AbstractControl): { [key: string]: any } | null {
    if (control.value) {
      const regex = /\d+/g;
      const onlyLetters = /[a-zA-Z]/;
      if (control.value !== '' || control.value !== undefined || control.value !== null) {
        if (!control.value.match(regex)) {
          return {passwordInvalid: true};
        }
        if (!control.value.match(onlyLetters)) {
          return {passwordInvalid: true};
        }
      }
      return null;
    }
    return null;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
        Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],
      username: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        this.validatePassword
      ]],
      remark: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
      // this.router.navigate(['/manage']);
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  get frmRemark() { return this.frm.get('remark'); }

}


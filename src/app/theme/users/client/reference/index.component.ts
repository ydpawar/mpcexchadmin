import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ActionService, ClientService} from '../../../../_api/index';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services/index';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;

interface User {
  id: string;
  name: string;
  username: string;
  plBalance: string;
  balance: string;
  mc: string;
  expose: string;
  available: string;
  pName: string;
  remark: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
  isLock: {
    text: string;
    class1: string;
    class2: string;
  };
}

class UserObj implements User {
  id: string;
  name: string;
  username: string;
  plBalance: string;
  balance: string;
  mc: string;
  expose: string;
  available: string;
  pName: string;
  remark: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
  isLock: {
    text: string;
    class1: string;
    class2: string;
  };
}


@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ClientService, ActionService]
})
export class ReferenceComponent implements OnInit, AfterViewInit, OnDestroy {

  page = { start: 1, end: 5 };

  uid: string;
  user: any;
  aList: User[] = [];
  cItem: User;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_client';

  title = 'Reference Client';
  createUrl = '/users/client/create';
  breadcrumb: any = [{title: 'Client', url: '/' }, {title: 'Reference', url: '/' }];

  constructor(
      private service: ClientService,
      private service2: ActionService,
      private route: ActivatedRoute,
      private spinner: NgxSpinnerService ) {
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }

    this.uid = this.route.snapshot.params.uid;
    // tslint:disable-next-line:triple-equals
    if ( window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined ) {
      this.title = 'Reference Client - ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem('title');
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.reference(this.uid).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_client').dataTable().fnDestroy();
        const items = res.data;
        const data: User[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: User = new UserObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.username = item.username;
            cData.plBalance = item.pl_balance;
            cData.mc = item.mc;
            cData.balance = item.balance;
            cData.expose = item.expose;
            cData.available = item.available;
            cData.remark = item.remark;
            cData.pName = item.pName;
            cData.isBlock = {
              text: item.isBlock === 1 ? 'Unblock' : 'Block',
              class1: item.isBlock === 1 ? 'success' : 'danger',
              class2: item.isBlock === 1 ? 'mdi-check' : 'mdi-close'
            };
            cData.isLock = {
              text: item.isLock === 1 ? 'Bet Unlock' : 'Bet Lock',
              class1: item.isLock === 1 ? 'success' : 'danger',
              class2: item.isLock === 1 ? 'mdi-toggle-switch' : 'mdi-toggle-switch-off'
            };

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
    this.spinner.hide();
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_client').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  doStatusUpdate(uid, typ) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if ( typ === 1 ) {
          this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        } else {
          this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        }
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}


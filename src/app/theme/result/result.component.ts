import { AfterViewInit, Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, MaxLengthValidator, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/common/commonComponent';
import { HistoryService } from 'src/app/_api';
import { AuthIdentityService } from 'src/app/_services';
import swal from 'sweetalert2';
import * as moment from 'moment'; // Momentjs
declare var $;
// tslint:disable-next-line:class-name
interface listData {
  marketId: string;
  eventId: string;
  date: string;
  result: string;
  description: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  marketId: string;
  eventId: string;
  date: string;
  result: string;
  description: string;
}

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
  providers: [HistoryService]
})
export class ResultComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  frm: FormGroup;
  title = 'Result';
  breadcrumb: any = [{ title: 'Result', url: '/' }, { title: 'List', url: '/' }];
  page = { start: 1, end: 5 };

  user: any;
  uid = '';
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  isDefault = true;
  dataTableId = 'DataTables_result';
  public filter: any ={};
  fType :string ='';
  constructor(
    inj: Injector,
    private service: HistoryService,
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.isDefault = false;
    const data = this.frm.value; data.isFirst = 1; data.ftype = 'today';
    this.service.result(this.uid, data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_result').dataTable().fnDestroy();
      if (res.userName !== undefined) {
        this.title = 'Result for ' + res.userName;
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.eventId = item.eventId;
            cData.marketId = item.marketId;
            cData.date = item.date;
            cData.description = item.description;
            cData.result = item.result;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        if (res.total) {
          this.aListTotal = res.total;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.initDataTables('DataTables_result');
      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', [Validators.required]],
      end: ['', [Validators.required]],
      // sysId: ['0'],
    });
  }

  submitForm(fType = null) {
    this.isDefault = false;
    if (fType != null) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType; data.isFirst = 0
      this.service.result(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value; data.isFirst = 0
        this.service.result(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  gameOver(eid, mId, closed, markettype?) {
    swal.fire({
      title: 'Confirm Game Over?',
      // text: title,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Winner Result',
        type: 'number',
        pattern: '[0-9]*',
      },
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, result, closed);
        }
      });

  }
  initGameOver(id, mId, results, closed) {
    const data = { eventId: id, marketId: mId, winResult: results.value, result: closed };
    console.log(data);
    // this.service2.gameOverMeter(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      // this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      // this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, eid, title?) {
    swal.fire({
      title: 'Confirm Game Recall?',
      // text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
        if (result.value) {
          this.initGameRecall(id, eid);
        }
      });
  }

  initGameRecall(id, eid) {
    const data = { marketId: id, eventId: eid};
    // this.service2.gameRecallMeter(data).subscribe((res) => this.onGameOver(res));
    console.log(data);
  }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
      const data={'start': sDate ,'end' : eDate}
      this.frm.patchValue(data);
      this.submitForm(this.fType);
  }
}

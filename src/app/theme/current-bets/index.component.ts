import {AfterViewInit, Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {HistoryService} from '../../_api';
import {Validators} from '@angular/forms';
import {BaseComponent} from '../../common/commonComponent';
import {ExcelService} from '../../common/excel.service';
import * as moment from 'moment'; // Momentjs
declare var $;

// tslint:disable-next-line:class-name
interface listData {
    id: object;
    oid: string;
    description: string;
    client: string;
    price: string;
    rate: string;
    stake: string;
    pl: string;
    result: string;
    type: string;
    ip: string;
    master: string;
    time: string;
    status: string;
    bgcolor: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
    id: object;
    oid: string;
    description: string;
    client: string;
    price: string;
    rate: string;
    stake: string;
    pl: string;
    result: string;
    type: string;
    ip: string;
    master: string;
    time: string;
    status: string;
    bgcolor: string;
}

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [HistoryService]
})

export class CurrentBetsComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    title = 'Current Bets';
    breadcrumb: any = [{title: 'Current Bets', url: '/'}];
    page = {start: 1, end: 5};
    uid = '';
    aList: listData[] = [];
    cItem: listData;
    isEmpty = false;
    isLoading = false;
    dataTableId = 'DataTables_current_bet';

    public filter: any ={};
    fType :string ='';
    constructor(
        inj: Injector,
        private service: HistoryService,
        private excelSheet: ExcelService
    ) {
        super(inj);
        this.uid = this.activatedRoute.snapshot.params.uid;
        // tslint:disable-next-line:triple-equals
        if (window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined) {
            this.title = 'Current Bets for ' + window.localStorage.getItem('title');
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.createForm();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        window.localStorage.removeItem('title');
        window.localStorage.removeItem(this.dataTableId);
    }

    applyFilters() {
        this.service.bets(this.uid, {}).subscribe((res) => this.onSuccess(res));
    }

    onSuccess(res) {
        this.spinner.hide();
        if (res.status !== undefined && res.status === 1) {
            $('#DataTables_current_bet').dataTable().fnDestroy();
            if (res.userName !== undefined) {
                this.title = 'Current Bets for ' + res.userName;
            }
            if (res.data !== undefined) {
                const items = res.data;
                const data: listData[] = [];

                if (items.length > 0) {
                    this.aList = [];
                    for (const item of items) {
                        const cData: listData = new listDataObj();

                        cData.id = item._id;
                        cData.oid = item._id.$oid;
                        cData.description = item.description;
                        cData.client = item.client;
                        cData.price = item.price;
                        // tslint:disable-next-line:max-line-length
                        if ( item.mType === 'set_market' || item.mType === 'goals' || item.mType === 'match_odd' || item.mType === 'fancy3' ) {
                            cData.rate = '-';
                        } else if ( item.mType === 'bookmaker' ) {
                            cData.price = item.rate;
                            cData.rate = '-';
                        } else {
                            cData.rate = item.rate;
                        }
                        cData.stake = item.size;
                        cData.result = item.result;
                        if (item.result === 'WIN') {
                            cData.pl = item.win;
                        } else if (item.result === 'LOSS') {
                            cData.pl = item.loss;
                        } else {
                            if (item.bType === 'back' || item.bType === 'yes' || item.bType === 'BACK' || item.bType === 'YES') {
                                cData.pl = item.win;
                            } else {
                                cData.pl = item.loss;
                            }
                        }
                        if (item.mType === 'meter') {
                            cData.pl = '-';
                        }
                        cData.type = item.bType;
                        cData.ip = item.ip_address;
                        cData.master = item.master;
                        cData.time = item.updated_on;
                        cData.status = item.status;
                        if ( item.bType === 'back' || item.bType === 'yes' || item.bType === 'BACK' || item.bType === 'YES' ) {
                            cData.bgcolor = 'bg-blue';
                        } else if ( item.bType === 'lay' || item.bType === 'no' || item.bType === 'LAY' || item.bType === 'NO' ) {
                            cData.bgcolor = 'bg-red';
                        }
                        // if ( item.status !== 1 ) {
                        //     cData.bgcolor = cData.bgcolor + ' line-through';
                        // } else { cData.bgcolor = cData.bgcolor; }
                        data.push(cData);
                    }
                } else {
                    this.isEmpty = true;
                }

                this.aList = data;
                this.page.end = this.page.end + items.length - 1;
                // this.loadJs.load('script' , 'assets/js/datatables.init.js');
                this.loadScript();

            }
        }
    }

    createForm() {
        this.frm = this.formBuilder.group({
            type: ['', Validators.required],
            start: ['', Validators.required],
            end: ['', Validators.required],
        });
    }

    submitForm(fType = null) {
        if (fType != null) {
            this.spinner.show();
            const data = this.frm.value;
            data.ftype = fType;
            this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
        } else {
            if (this.frm.valid) {
                this.spinner.show();
                const data = this.frm.value;
                this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
            }
        }
    }

    onSearch(res) {
        if (res.status === 1) {
            // this.frm.reset();
            this.onSuccess(res);
        }
    }

    loadScript() {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function() {
            $('#DataTables_current_bet').DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });

    }

    excelDownload() {
        const tmpData = [];
        let tmp = {
            BET_ID: '',
            DESCRIPTION: '',
            CLIENT: '',
            PRICE: '',
            RATE: '',
            STAKE: '',
            TYPE: '',
            PL: '',
            RESULT: '',
            IP: '',
            MASTER: '',
            TIME: '',
        };
        tmpData.push(tmp);
        this.aList.forEach((item, index) => {
            tmp = {
                BET_ID: item.oid,
                DESCRIPTION: item.description,
                CLIENT: item.client,
                PRICE: item.price,
                RATE: item.rate,
                STAKE: item.stake,
                TYPE: item.type,
                PL: item.pl,
                RESULT: item.result,
                IP: item.ip,
                MASTER: item.master,
                TIME: item.time,
            };
            tmpData.push(tmp);
        });
        this.excelSheet.exportAsExcelFile(tmpData, 'bet-list');
    }

    get frmType() { return this.frm.get('type'); }
    get frmStart() { return this.frm.get('start'); }
    get frmEnd() { return this.frm.get('end'); }

    filters(search: string) {
        this.fType = search;
        this.filter.isFirst = 0;
        switch (search) {
          case 'today' :
            this.filter.start_date = moment().format('YYYY-MM-DD');
            this.filter.end_date = moment().format('YYYY-MM-DD');
            this.setInputDate(this.filter.start_date, this.filter.end_date);
            break;
          case 'yesterday' :
            this.filter.end_date = moment().format('YYYY-MM-DD');
            this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
            this.setInputDate(this.filter.start_date, this.filter.end_date);
            break;
          case 'week' :
            this.filter.end_date = moment().format('YYYY-MM-DD');
            this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
            this.setInputDate(this.filter.start_date, this.filter.end_date);
            break;
          case 'month' :
            this.filter.end_date = moment().format('YYYY-MM-DD');
            this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
            this.setInputDate(this.filter.start_date, this.filter.end_date);
            break;
          case '3_month' :
            this.filter.end_date = moment().format('YYYY-MM-DD');
            this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
            this.setInputDate(this.filter.start_date, this.filter.end_date);
            break;
        }
        // this.filterChanged.emit(this.filter);
      }
    
      setInputDate(sDate: Date, eDate: Date) {
          const data={'start': sDate ,'end' : eDate}
          this.frm.patchValue(data);
          this.submitForm(this.fType);
      }

    getLastChar(id: any) {
        return id.substr(id.length - 6);
    }

}

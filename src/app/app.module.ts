import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { BaseComponent } from './common/commonComponent';

import { HttpClientModule } from '@angular/common/http';
import { AuthIdentityService, HttpService, ScriptLoaderService } from './_services/index';
import { ToastrService } from './_services/toastr.service';
import {AuthGuard, AuthGuardChild, CanAuthActivate, CanLoginActivate} from './_guards/auth.guard';

import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HrefPreventDefaultDirective} from './_directives/href-prevent-default.directive';
import {SafePipe} from './_directives/safe.pipe';
import {UnwrapTagDirective} from './_directives/unwrap-tag.directive';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [CanLoginActivate]
  },
  {
    path: '',
    loadChildren: './theme/theme.module#ThemeModule',
    canActivate: [CanAuthActivate],
  }
];

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    LoginComponent,
    SafePipe,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [HttpService, ToastrService,AuthIdentityService, AuthGuard, AuthGuardChild,CanAuthActivate, CanLoginActivate, ScriptLoaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }

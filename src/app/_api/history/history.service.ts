import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable()
export class HistoryService {
  constructor(private http: HttpService) { }

  activityLog(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    httpRequestModel.url = 'history/activity-log';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  chips(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'history/chip-history/' + id;
    } else {
      httpRequestModel.url = 'history/chip-history';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  bets(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'history/bet-history/' + id;
    } else {
      httpRequestModel.url = 'history/bet-history';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  doBetDelete(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'dashboard/event/bet-delete';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  marketBets(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    httpRequestModel.url = 'dashboard/market-bet-list';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemMarketBets(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    httpRequestModel.url = 'dashboard/system-market-bet-list';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLoss(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemProfitLoss(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/system-profit-loss/' + id;
    } else {
      httpRequestModel.url = 'transaction/system-profit-loss';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLossEvent(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss-by-event/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss-by-event';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemProfitLossEvent(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/system-profit-loss-by-event/' + id;
    } else {
      httpRequestModel.url = 'transaction/system-profit-loss-by-event';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  profitLossMarket(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/profit-loss-by-market/' + id;
    } else {
      httpRequestModel.url = 'transaction/profit-loss-by-market';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemProfitLossMarket(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'transaction/system-profit-loss-by-market/' + id;
    } else {
      httpRequestModel.url = 'transaction/system-profit-loss-by-market';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  teenpattiProfitLoss(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/teenpatti-profit-loss';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  casinoProfitLoss(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/casino-profit-loss';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemTeenpattiProfitLoss(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/system-teenpatti-profit-loss';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemCasinoProfitLoss(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/system-casino-profit-loss';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  accountStatement(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    if (id) {
      httpRequestModel.url = 'transaction/account-statement/' + id;
    } else {
      httpRequestModel.url = 'transaction/account-statement';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  accountStatementMonth(date, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/account-statement-month/' + date;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  accountStatementMonthMarket(date, eid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'transaction/account-statement-month-market/' + eid + '?month=' + date;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  result(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'market-result/' + id;
    } else {
      httpRequestModel.url = 'market-result';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

}
